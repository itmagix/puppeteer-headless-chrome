
  
'use strict';

const puppeteer = require('puppeteer');

(async () => {
	  const browser = await puppeteer.launch({
	      headless: true,
	      args: [
		      "--disable-gpu",
		      "--disable-dev-shm-usage",
		      "--disable-setuid-sandbox",
		      "--no-sandbox",
	      ]
	  });
	  const page = await browser.newPage();
/*	  await page.emulate(puppeteer.devices['iPhone 6']); */
	  await page.goto('https://www.itmagix.nl/');
	  await page.screenshot({ path: '/output/full.png', fullPage: true });
	  await browser.close();
});
