# Docker image with latest Puppeteer, Headless Chrome and Node versions
## Official links
[Puppeteer website](https://pptr.dev) - Official Puppeteer Website<br>
[Puppeteer Google developers](https://devlopers.google.com/web/tools/puppeteer) - Official homepage at Google Developers<br>
[API](https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md) - Official API documentation<br>
[Hosted Playground](https://try-puppeteer.appspot.com) - A playground where you can try and experiment with Puppeteer<br>
[FAQ](https://developers.google.com/web/tools/puppeteer/#faq) - Official F.A.Q. at Google Developers<br>
[DevTools Protocol](https://chromedevtools.github.io/devtools-protocol/) - Chrome DevTools Protocol API Docs<br>

## Github links
[Github page Puppeteer](https://github.com/puppeteer/puppeteer)<br>
[Headless Chrome](https://developers.google.com/web/updates/2017/04/headless-chrome)<br>
[Node](https://github.com/nodejs/node)<br>

## Puppeteer
> Puppeteer is a Node library which provides a high-level API to control Chrome or Chromium over the [DevTools Protocol](https://chromedevtools.github.io/devtools-protocol/). Puppeteer runs [headless](https://developers.google.com/web/updates/2017/04/headless-chrome) by default, but can be configured to run full (non-headless) Chrome or Chromium.

### What can I do?
Most things that you can do manually in the browser can be doen using Puppeteer! Here are a few examples to get you started:

- Generate screenshots and PDFs of pages
- Crawl a SPA (Single-Page Application) and generate pre-rendered content (i.e. "SSR" (Server-Side Rendering))
- Automate form submission, UI testing, keyboard input, etc
- Create an up-to-date, automated testing environment. Run your tests directly in the latest version of Chrome using the latest JavaScript and browser features
- Capture a [timeline trace](https://developers.google.com/web/tools/chrome-devtools/evaluate-performance/reference) of your site to help diagnose performance issues
- Test Chrome Extensions

###  Getting started
This is a customized version Docker image to run Headless Chrome with the latest versions of Node, Chrome and Puppeteer from within a CI/CD pipeline.

#### Example
There is a simple example script in the example folder which you can use to try Puppeteer by using the following command;
`docker run -i --init --rm --cap-add=SYS_ADMIN --name puppeteer-chrome puppeteer-headless-chrome:latest  node -e "\`cat example/puppeteer-example-itmagix.js\`"`

#### Example resources
The following link provides an overview of resources with more (advanced) script examples and much more to automate your projects. 
[Awesome Puppeteer](https://github.com/transitive-bullshit/awesome-puppeteer)

